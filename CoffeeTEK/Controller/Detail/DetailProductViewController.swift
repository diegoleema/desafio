//
//  DetailProductViewController.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import UIKit

class DetailProductViewController: BaseViewController {
    
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var imageSpotlight: UIImageView!
    var productSelected:Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbView.delegate = self
        self.tbView.dataSource = self
        self.loadData()
        self.loadXib()
    }
    
    func loadData() {
        let imageEncoded = self.productSelected!.image?.components(separatedBy: ",")
        if let decodedData = Data(base64Encoded: imageEncoded![1], options: .ignoreUnknownCharacters) {
            self.imageSpotlight.image = UIImage(data: decodedData)
        }
    }
    
    func loadXib() {
        let presaleNIB = UINib(nibName: "InfoProductTableViewCell", bundle: nil)
        self.tbView.register(presaleNIB, forCellReuseIdentifier: "cellInfoProduct")
        
        let presaleNIB2 = UINib(nibName: "SizeProductTableViewCell", bundle: nil)
        self.tbView.register(presaleNIB2, forCellReuseIdentifier: "cellSizeProduct")
        
        let presaleNIB3 = UINib(nibName: "SugarProductTableViewCell", bundle: nil)
        self.tbView.register(presaleNIB3, forCellReuseIdentifier: "cellSugarProduct")
        
        let presaleNIB4 = UINib(nibName: "AdditionalProductTableViewCell", bundle: nil)
        self.tbView.register(presaleNIB4, forCellReuseIdentifier: "cellAdditionalProduct")
        
        let presaleNIB5 = UINib(nibName: "QtdProductTableViewCell", bundle: nil)
        self.tbView.register(presaleNIB5, forCellReuseIdentifier: "cellQtdProduct")
    }

    @IBAction func addToCart(_ sender: UIButton) {
        if ItemCart.sharedInstance.qtdProduct == 0 {
            self.openAlert(title: "Erro!", message: "É obrigatório ter no mínimo um item para adicionar ao carrinho")
        } else {
            ItemCart.sharedInstance.title = self.productSelected?.title
            ItemCart.sharedInstance.additional = self.productSelected?.additional
            ItemCart.sharedInstance.image = self.productSelected?.image
            let itemToCart = ItemCart(item: ItemCart.sharedInstance)
            ShoppingCart.sharedInstance.addItemCart(itemCart: itemToCart)
            performSegue(withIdentifier: "sgDetailShoppingCart", sender: nil)
        }
    }
}

extension DetailProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        ItemCart.sharedInstance.updateItemCart(qtdProduct: 0, sizeProduct: (self.productSelected?.size)!, sugarProduct: (self.productSelected?.sugar)!)
        switch indexPath.row {
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellInfoProduct", for: indexPath) as! InfoProductTableViewCell
            cell.nameProduct.text = self.productSelected?.title
            cell.contentProduct.text = "\(Int(((self.productSelected?.size)! + 1) * 200)) ML"
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSizeProduct", for: indexPath) as! SizeProductTableViewCell
            cell.size = (self.productSelected?.size)!
            cell.updateSelected()
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSugarProduct", for: indexPath) as! SugarProductTableViewCell
            cell.sugar = (self.productSelected?.sugar)!
            cell.updateSelected()
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellAdditionalProduct", for: indexPath) as! AdditionalProductTableViewCell
            
            if let additional = self.productSelected?.additional {
                cell.additional = additional
            }
           
            cell.loadData()
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellQtdProduct", for: indexPath) as! QtdProductTableViewCell
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    
}
