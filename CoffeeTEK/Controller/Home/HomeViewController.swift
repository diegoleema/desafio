//
//  ViewController.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 08/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import UIKit
import SVProgressHUD

class HomeViewController: BaseViewController, UITabBarControllerDelegate {
    
    @IBOutlet weak var tbView: UITableView!
    var listProduct:ListProduct?
    var productSelected:Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbView.delegate = self
        self.tbView.dataSource = self
        self.loadXib(nib: "ProductTableViewCell", identifier: "cellProduct")
        self.getAllProducts()
    }
    
    func getAllProducts() {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.show()
        ConnectionManager.sharedInstance.listProducts() { (error, data, status) -> Void in
            if data != nil {
                if let products = data!["products"] as? NSArray {
                    self.listProduct = ListProduct.init(products: Product.decode(products) as! [Product])
                    self.tbView.reloadData()
                }
            } else {
                if status == 503 {
                    self.openAlert(title: "Falha de conexão!", message: "Verifique sua conexão com a internet e tente novamente!")
                } else {
                    self.openAlert(title: "Erro inesperado!", message: "O serviço pode estar temporariamente indisponível, favor tentar novamente mais tarde.")
                }
            }
            SVProgressHUD.dismiss()
        }
    }
    
    func loadXib(nib:String, identifier:String) {
        let presaleNIB = UINib(nibName: nib, bundle: nil)
        self.tbView.register(presaleNIB, forCellReuseIdentifier: identifier)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "sgDetailProduct") {
            let vc = segue.destination as! DetailProductViewController
            vc.productSelected = self.productSelected
        }
    }
    @IBAction func tapCart(_ sender: Any) {
        performSegue(withIdentifier: "sgShoppingCart", sender: nil)
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.listProduct == nil {
            return 0
        }
        return self.listProduct!.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellProduct", for: indexPath) as! ProductTableViewCell
        let product = self.listProduct!.products[indexPath.row]
        cell.nameProduct.text = product.title
        
        let imageEncoded = product.image?.components(separatedBy: ",")
        if let decodedData = Data(base64Encoded: imageEncoded![1], options: .ignoreUnknownCharacters) {
            cell.imgProduct.image = UIImage(data: decodedData)
        }
        
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.productSelected = self.listProduct?.products[indexPath.row]
        performSegue(withIdentifier: "sgDetailProduct", sender: nil)
    }
}

