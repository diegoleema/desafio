//
//  ShoppingCartViewController.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import UIKit

class ShoppingCartViewController: UIViewController {

    @IBOutlet weak var vwEmptyState: UIView!
    @IBOutlet weak var tbView: UITableView!
    var items: [ItemCart] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbView.delegate = self
        self.tbView.dataSource = self
        self.loadXib()
        self.loadData()
    }
    
    func loadData() {
        self.items = ShoppingCart.sharedInstance.items
        if self.items.isEmpty {
            self.vwEmptyState.alpha = 1
            self.tbView.isHidden = true
        } else {
            self.vwEmptyState.alpha = 0
            self.tbView.isHidden = false
        }
        self.tbView.reloadData()
    }
    
    func loadXib() {
        let presaleNIB = UINib(nibName: "ItemCartTableViewCell", bundle: nil)
        self.tbView.register(presaleNIB, forCellReuseIdentifier: "cellItemCart")
    }
}

extension ShoppingCartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellItemCart", for: indexPath) as! ItemCartTableViewCell
        cell.nameProduct.text = self.items[indexPath.row].title
        cell.qtdProduct.text = "\((self.items[indexPath.row].qtdProduct)!)"
        cell.additionalsProduct.text = self.items[indexPath.row].additional?.joined(separator: ", ")
        
        let imageEncoded = self.items[indexPath.row].image?.components(separatedBy: ",")
        if let decodedData = Data(base64Encoded: imageEncoded![1], options: .ignoreUnknownCharacters) {
            cell.imageProduct.image = UIImage(data: decodedData)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
}
