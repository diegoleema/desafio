//
//  ModelsExtension.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import Foundation

extension Product {
    static func decode(_ json: NSArray) -> NSArray {
        var results:[Product] = []
        for item in json {
            let product = Product(dictionary: item as! NSDictionary)
            results.append(product)
        }
        return results as NSArray
    }
}
