//
//  listProduct.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 08/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import Foundation

class ListProduct : NSObject {
    var products: [Product]
    
    init(products: [Product]) {
        self.products = products
    }
}


