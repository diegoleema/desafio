//
//  Product.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 08/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import Foundation

class Product : NSObject {
    var title, image: String?
    var size, sugar: Int?
    var additional: [String]?
    
    override init() {}
    
    init(dictionary: NSDictionary) {
        super.init()
        for (key,value) in dictionary {
            let keyName = key as! String
            let keyValue = value
            
            
            if !(keyValue is NSNull) {
                if let value = keyValue as? String {
                    if keyName.lowercased() == "title" {
                        self.title = keyValue as! String
                    } else if keyName.lowercased() == "image" {
                        self.image = keyValue as! String
                    }
                } else {
                    if keyName.lowercased() == "size" {
                        self.size = keyValue as! Int
                    } else if keyName.lowercased() == "sugar" {
                        self.sugar = keyValue as! Int
                    } else if keyName.lowercased() == "additional" {
                        self.additional = keyValue as! [String]
                    }
                }
            }
            
        }
    }
}
