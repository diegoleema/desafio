//
//  ItemCart.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import Foundation

class ItemCart : NSObject {
    static let sharedInstance = ItemCart()
    var qtdProduct: Int?
    var sizeProduct: Int?
    var sugarProduct: Int?
    var title: String?
    var additional: [String]?
    var image: String?
    
    private override init () { }
    
    init(item: ItemCart) {
        self.qtdProduct = item.qtdProduct
        self.sizeProduct = item.sizeProduct
        self.sugarProduct = item.sugarProduct
        self.title = item.title
        self.additional = item.additional
        self.image = item.image
    }
    
    func updateItemCart(qtdProduct: Int, sizeProduct: Int, sugarProduct: Int) {
        self.qtdProduct = qtdProduct
        self.sizeProduct = sizeProduct
        self.sugarProduct = sugarProduct
    }
}
