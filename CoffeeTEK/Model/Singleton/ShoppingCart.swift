//
//  ShoppingCart.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import Foundation

class ShoppingCart : NSObject {
    static let sharedInstance = ShoppingCart()
    
    var items:[ItemCart] = []
    
    private override init () { }
    
    func addItemCart(itemCart: ItemCart) {
        self.items.append(itemCart)
    }
}
