//
//  ConnectionManager.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 08/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import Foundation
import Alamofire

class ConnectionManager {
    static let sharedInstance = ConnectionManager()
    
    func listProducts(completion:@escaping (_ error: Error?, _ value: AnyObject?, _ status: Int?) -> Void) {
        let endPoint: String = CoffeTEK.Routes.listProduct
        
        Alamofire.request(endPoint, method: .get).responseJSON { response in
            completion(response.result.error, response.result.value as AnyObject?, response.response?.statusCode)
        }
    }
    
}
