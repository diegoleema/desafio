//
//  Constants.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 08/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import Foundation

struct CoffeTEK {
    
    struct Routes {
        static let listProduct = "https://desafio-mobility.herokuapp.com/products.json"
    }
    
    struct Colors {
        static let titleDefault = "#58443e"
    }
}
