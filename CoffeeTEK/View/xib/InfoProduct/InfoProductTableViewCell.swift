//
//  InfoProductTableViewCell.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import UIKit

class InfoProductTableViewCell: UITableViewCell {

    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var contentProduct: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
