//
//  ItemCartTableViewCell.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import UIKit

class ItemCartTableViewCell: UITableViewCell {

    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var qtdProduct: UILabel!
    @IBOutlet weak var additionalsProduct: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
