//
//  AdditionalProductTableViewCell.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import UIKit

class AdditionalProductTableViewCell: UITableViewCell {
    @IBOutlet weak var textAdditional: UILabel!
    var additional: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData() {
        var positionX = self.textAdditional.frame.width + 30.0
        if !self.additional.isEmpty {
            for item in additional {
                var imageView : UIImageView
                imageView  = UIImageView(frame: CGRect(x: positionX, y: self.textAdditional.center.y, width: 20, height: 20));
                imageView.image = UIImage(named: item)
                positionX = positionX + (imageView.frame.width + 20.0)
                self.addSubview(imageView)
            }
        }
    }
    
}
