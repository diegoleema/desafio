//
//  SizeProductTableViewCell.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import UIKit

class SizeProductTableViewCell: UITableViewCell {

    @IBOutlet weak var btnSize1: UIButton!
    @IBOutlet weak var btnSize2: UIButton!
    @IBOutlet weak var btnSize3: UIButton!
    var size: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateSelected() {
        self.setAllOpacity()
        ItemCart.sharedInstance.sizeProduct = size
        switch size {
        case 0:
            self.btnSize1.alpha = 1
            break
        case 1:
            self.btnSize2.alpha = 1
            break
        case 2:
            self.btnSize3.alpha = 1
            break
        default:
            break
        }
    }
    
    func setAllOpacity() {
        self.btnSize1.alpha = 0.4
        self.btnSize2.alpha = 0.4
        self.btnSize3.alpha = 0.4
    }
    
    @IBAction func didTapBtn(_ sender: UIButton) {
        switch sender {
        case self.btnSize1:
            self.size = 0
            break
        case self.btnSize2:
            self.size = 1
            break
        case self.btnSize3:
            self.size = 2
            break
        default:
            break
        }
        self.updateSelected()
    }
    
}
