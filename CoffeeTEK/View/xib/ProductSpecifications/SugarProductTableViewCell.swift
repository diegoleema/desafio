//
//  SugarProductTableViewCell.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import UIKit

class SugarProductTableViewCell: UITableViewCell {

    @IBOutlet weak var btnSugar0: UIButton!
    @IBOutlet weak var btnSugar1: UIButton!
    @IBOutlet weak var btnSugar2: UIButton!
    @IBOutlet weak var btnSugar3: UIButton!
    var sugar: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateSelected() {
        self.setAllOpacity()
        ItemCart.sharedInstance.sugarProduct = sugar
        switch sugar {
        case 0:
            self.btnSugar0.alpha = 1
            break
        case 1:
            self.btnSugar1.alpha = 1
            break
        case 2:
            self.btnSugar2.alpha = 1
            break
        case 3:
            self.btnSugar3.alpha = 1
            break
        default:
            break
        }
    }
    
    func setAllOpacity() {
        self.btnSugar0.alpha = 0.4
        self.btnSugar1.alpha = 0.4
        self.btnSugar2.alpha = 0.4
        self.btnSugar3.alpha = 0.4
    }
    
    @IBAction func didTapBtn(_ sender: UIButton) {
        switch sender {
        case self.btnSugar0:
            self.sugar = 0
            break
        case self.btnSugar1:
            self.sugar = 1
            break
        case self.btnSugar2:
            self.sugar = 2
            break
        case self.btnSugar3:
            self.sugar = 3
            break
        default:
            break
        }
        self.updateSelected()
    }
    
}
