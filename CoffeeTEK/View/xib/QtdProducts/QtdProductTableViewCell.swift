//
//  QtdProductTableViewCell.swift
//  CoffeeTEK
//
//  Created by Diego Lima on 09/01/19.
//  Copyright © 2019 Diego Lima. All rights reserved.
//

import UIKit

class QtdProductTableViewCell: UITableViewCell {

    @IBOutlet weak var btnLessQtd: UIButton!
    @IBOutlet weak var btnPlusQtd: UIButton!
    @IBOutlet weak var qtdProduct: UILabel!
    var qtdActual: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.qtdActual = 0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didTapLessQtd(_ sender: UIButton) {
        
        if (self.qtdActual! > 0) {
            self.qtdActual = self.qtdActual! - 1
            self.qtdProduct.text = "\(self.qtdActual!)"
            self.updateQtdProduct(qtd: self.qtdActual!)
        }
    }
    @IBAction func didTapPlusQtd(_ sender: UIButton) {
        if (self.qtdActual! < 10) {
            self.qtdActual = self.qtdActual! + 1
            self.qtdProduct.text = "\(self.qtdActual!)"
            self.updateQtdProduct(qtd: self.qtdActual!)
        }
    }
    
    func updateQtdProduct(qtd: Int) {
        ItemCart.sharedInstance.qtdProduct = qtd
    }
}
